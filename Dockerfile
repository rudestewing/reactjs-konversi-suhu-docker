FROM node:16-alpine

WORKDIR /app


COPY package.json .
COPY package-lock.json .

RUN yarn install

COPY . .

CMD ["npm", "run", "dev"]
